<?php

namespace MyRepublic\Mobile\Services;

use function GuzzleHttp\json_encode;
use MyRepublic\Mobile\Contracts\MobileServicesInterface;
use MyRepublic\Mobile\Entities\MobileNumber;
use MyRepublic\Mobile\Entities\MobileStatus;
use MyRepublic\Mobile\Entities\MobileStatusHistory;
use MyRepublic\Mobile\Entities\MobileSubscription;
use MyRepublic\Mobile\Entities\MobileSubscriptionHistory;
use MyRepublic\Mobile\Entities\MobileSubscriptionStatus;
use MyRepublic\Mobile\Entities\MobileUpdateLog;
use MyRepublic\Mobile\Query\MobileNumberQuery;

/**
 * Mobile Service class
 */
class MobileService implements MobileServicesInterface
{
    /**
     * Subscribe a given phone number to a service
     *
     * @param [type] $data
     * @return void
     */
    public function subscribe($data)
    {
        $mobileNumber         = MobileNumber::where('phone_number', $data['phone_number'])->first(['id']);
        $mobileId             = $mobileNumber->id;
        $subscriptionId       = $data['subscription_type'];
        $subscriptionStatusId = $data['status'];
        $username             = $data['username'];

        try {

            // validate if phone number already subscribed to the server, then update status
            $oldData = null;
            if ($oldData = (new MobileNumberQuery())->checkPhoneSubscription($mobileId, $subscriptionId)) {
                $newData            = $oldData;
                $newData->status_id = $subscriptionStatusId;
                $newData->save();
            } else {
                $oldData = [];

                $newData                       = new MobileSubscription();
                $newData->mobile_id            = $mobileId;
                $newData->subscription_type_id = $subscriptionId;
                $newData->status_id            = $subscriptionStatusId;
                $newData->save();

                //Set Mobile Status to active
                MobileStatusHistory::where('mobile_id', $mobileId)->update(['is_current' => 0]);
                MobileStatusHistory::create([
                    'mobile_id'  => $mobileId,
                    'status_id'  => MobileStatus::STATUS_ACTIVE,
                    'is_current' => 1
                ]);
            }

            // Update subscription history
            MobileSubscriptionHistory::create([
                'mobile_id'            => $mobileId,
                'subscription_type_id' => $subscriptionId,
                'status_id'            => $subscriptionStatusId,
                'admin_username'       => $username
            ]);

            // Update mobile update logs
            MobileUpdateLog::create([
                'username'    => $username,
                'transaction' => 'Mobile service subscription update',
                'from_data'   => json_encode($oldData),
                'to_data'     => json_encode($newData)
            ]);

            // Retrieve full mobile details
            return (new MobileNumberQuery())->getPhoneNumberDetails($data['phone_number']);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Cancell all existing active mobile service subscription
     *
     * @param [type] $data
     * @return void
     */
    public function unSubscribe($data)
    {
        $phoneNumber = $data['phone_number'];
        $username    = $data['username'];
        $statusId    = MobileSubscriptionStatus::STATUS_CANCELLED;
        try {
            $subscriptions = (new MobileNumberQuery())->getPhoneSubscription($phoneNumber);
            foreach ($subscriptions as $subs) {
                $oldData         = $subs;
                $subs->status_id = $statusId;
                $subs->save();
                $newData = $subs;

                // Update subscription history
                MobileSubscriptionHistory::create([
                    'mobile_id'            => $subs->mobile_id,
                    'subscription_type_id' => $subs->subscription_type_id,
                    'status_id'            => $statusId,
                    'admin_username'       => $username
                ]);

                // Update mobile update logs
                MobileUpdateLog::create([
                    'username'    => $username,
                    'transaction' => 'Mobile service cancellation',
                    'from_data'   => json_encode($oldData),
                    'to_data'     => json_encode($newData)
                ]);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
