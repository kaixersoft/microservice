<?php

namespace MyRepublic\Mobile\Services;

use MyRepublic\Mobile\Contracts\MobileNumberInterface;
use MyRepublic\Mobile\Contracts\MobileServicesInterface;
use MyRepublic\Mobile\Entities\MobileNumber;
use MyRepublic\Mobile\Entities\MobileStatus;
use MyRepublic\Mobile\Entities\MobileStatusHistory;
use MyRepublic\Mobile\Entities\MobileSubscription;
use MyRepublic\Mobile\Entities\MobileUpdateLog;
use MyRepublic\Mobile\Query\MobileNumberQuery;
use MyRepublic\Mobile\Transformers\MobileNumberDetailsTransformer;
use MyRepublic\Mobile\Transformers\MobileNumberHistoryTransformer;
use MyRepublic\Mobile\Transformers\MobileNumberWithStatusTransformer;

/**
 * Mobile Number Service
 */
class MobileNumberService implements MobileNumberInterface
{
    /**
     * Get All mobile number with current status
     *
     */
    public function getAllMobileNumberWithCurrentStatus($request)
    {
        $query = new MobileNumberQuery();
        $data  = $query::run($request);

        return MobileNumberWithStatusTransformer::collection($data);
    }

    /**
     * Get all details about phone number
     *
     * @param [type] $phoneNumber
     * @return void
     */
    public function getMobileNumberDetails($phoneNumber)
    {
        $query = new MobileNumberQuery();
        $data  = $query->getPhoneNumberDetails($phoneNumber);

        return MobileNumberDetailsTransformer::collection($data);
    }

    /**
     * Get all history details of a phone number
     *
     * @param [type] $phoneNumber
     * @return void
     */
    public function getMobileNumberHistory($phoneNumber)
    {
        $query = new MobileNumberQuery();
        $data  = $query->getPhoneNumberHistory($phoneNumber);

        return MobileNumberHistoryTransformer::collection($data);
    }

    /**
     * Terminate mobile number
     *
     * @param string $phoneNumber
     * @return void
     */
    public function blockMobileNumber($phoneNumber, $request)
    {
        try {
            // Cancel any mobile subscriptions
            $mobileService = resolve(MobileServicesInterface::class);
            $mobileService->unSubscribe(['phone_number' => $phoneNumber, 'username' => $request['username']]);

            // Terminate mobile number
            $statusId            = MobileStatus::STATUS_TERMINATED;
            $mobileNumber        = MobileNumber::where('phone_number', $phoneNumber)->first();
            $currentMobileStatus = MobileStatusHistory::where('mobile_id', $mobileNumber->id)
                                    ->where('is_current', 1)->lockForUpdate()->first();
            $oldData = $currentMobileStatus;
            // Set new status
            $newData = MobileStatusHistory::create([
                'mobile_id'  => $mobileNumber->id,
                'status_id'  => $statusId,
                'is_current' => 1
            ]);

            // Revoke current status
            $currentMobileStatus->is_current = 0;
            $currentMobileStatus->save();

            // Update logs
            // Update mobile update logs
            MobileUpdateLog::create([
                'username'    => $request['username'],
                'transaction' => 'Mobile service terminated',
                'from_data'   => json_encode($oldData),
                'to_data'     => json_encode($newData)
            ]);

            // Return new status details of the number
            return $this->getMobileNumberDetails($phoneNumber);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Re-Activate mobile number
     *
     * @param string $phoneNumber
     * @return void
     */
    public function unBlockMobileNumber($phoneNumber, $request)
    {
        try {
            $mobileService = resolve(MobileServicesInterface::class);

            // Activate mobile number
            $statusId     = MobileStatus::STATUS_AVAILABLE;
            $mobileNumber = MobileNumber::where('phone_number', $phoneNumber)->first();

            // Check if phone number is already active
            $currentStatus = (new MobileNumberQuery())->phoneNumberIsActive($phoneNumber);

            if ($currentStatus != 'active') {

                // remove any current subscription from previous number owner
                MobileSubscription::where('mobile_id', $mobileNumber->id)->delete();

                $oldData = [];

                // Revoke any current status
                MobileStatusHistory::where('mobile_id', $mobileNumber->id)->update(['is_current' => 0]);

                // Set new status
                $newData = MobileStatusHistory::create([
                    'mobile_id'  => $mobileNumber->id,
                    'status_id'  => $statusId,
                    'is_current' => 1
                    ]);

                // Update logs
                MobileUpdateLog::create([
                    'username'    => $request['username'],
                    'transaction' => 'Mobile service activated',
                    'from_data'   => json_encode($oldData),
                    'to_data'     => json_encode($newData)
                ]);
            }
            // Return new status details of the number
            return $this->getMobileNumberDetails($phoneNumber);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
