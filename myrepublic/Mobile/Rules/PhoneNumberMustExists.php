<?php

namespace MyRepublic\Mobile\Rules;

use Illuminate\Contracts\Validation\Rule;
use MyRepublic\Mobile\Entities\MobileNumber;

class PhoneNumberMustExists implements Rule
{
    protected $phoneNumber;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return MobileNumber::where('phone_number', $this->phoneNumber)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid phone number or number does not exists';
    }
}
