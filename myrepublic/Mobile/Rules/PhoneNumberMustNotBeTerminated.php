<?php

namespace MyRepublic\Mobile\Rules;

use Illuminate\Contracts\Validation\Rule;
use MyRepublic\Mobile\Query\MobileNumberQuery;

class PhoneNumberMustNotBeTerminated implements Rule
{
    protected $phoneNumber;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $currentStatus = (new MobileNumberQuery())->phoneNumberIsActive($this->phoneNumber);

        return ($currentStatus != 'terminated') ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Phone number must be active or available';
    }
}
