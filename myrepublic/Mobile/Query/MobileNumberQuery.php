<?php

namespace MyRepublic\Mobile\Query;

use Illuminate\Support\Facades\DB;
use MyRepublic\Mobile\Entities\MobileNumber;
use MyRepublic\Mobile\Entities\MobileSubscription;
use MyRepublic\Mobile\Entities\MobileSubscriptionStatus;

/**
 * DB Queries for mobile number
 */
class MobileNumberQuery
{
    const LIMIT = 50;

    /**
     * Run Query against Mobile Number
     *
     * @param [type] $request
     * @return void
     */
    public static function run(array $request)
    {
        $baseQuery        = (new MobileNumber())->newQuery();
        $currentStatusSql = "(select mobile_id, status_id from mobile_status_histories where is_current =1) as current_status";

        $status = isset($request['status']) ? $request['status'] : null;
        $phone  = isset($request['phone']) ? $request['phone'] : null;

        if (empty($status)) {
            $baseQuery = (clone $baseQuery)
                ->leftJoin(\DB::raw($currentStatusSql), function ($join) {
                    $join->on('current_status.mobile_id', '=', 'mobile_numbers.id');
                })
                ->leftJoin('mobile_statuses', 'mobile_statuses.id', '=', 'current_status.status_id');
        } else {
            $statusId = $status;

            if ($statusId != '1') {
                $baseQuery = (clone $baseQuery)
                ->join(\DB::raw($currentStatusSql), function ($join) {
                    $join->on('current_status.mobile_id', '=', 'mobile_numbers.id');
                })
                ->join('mobile_statuses', 'mobile_statuses.id', '=', 'current_status.status_id')
                ->where(function ($query) use ($statusId) {
                    return $query->where('current_status.status_id', $statusId);
                });
            } else {
                $baseQuery = (clone $baseQuery)
                    ->leftJoin(\DB::raw($currentStatusSql), function ($join) {
                        $join->on('current_status.mobile_id', '=', 'mobile_numbers.id');
                    })
                    ->leftJoin('mobile_statuses', 'mobile_statuses.id', '=', 'current_status.status_id')
                    ->whereNull('current_status.status_id');
            }
        }

        $orderBy = $baseQuery->orderBy('mobile_numbers.id');

        if (!empty($phone)) {
            $baseQuery = $baseQuery->where('mobile_numbers.phone_number', 'LIKE', '%' . $phone . '%')
                           ->orderBy('mobile_numbers.phone_number');
        }

        $pageLimit = isset($request['limit']) ? $request['limit'] : self::LIMIT;

        $baseQuery = $baseQuery->select([
            'mobile_numbers.phone_number',
            DB::raw('COALESCE(mobile_statuses.description, \'available\') as status')
        ]);

        $baseQuery = $baseQuery->paginate($pageLimit);

        return $baseQuery;
    }

    /**
     * Get Phone Number current status
     */
    public function phoneNumberIsActive($phoneNumber)
    {
        $currentStatusSql = "(select mobile_id, status_id from mobile_status_histories where is_current =1) as current_status";
        $baseQuery        = (new MobileNumber())->newQuery()
            ->leftJoin(\DB::raw($currentStatusSql), function ($join) {
                $join->on('current_status.mobile_id', '=', 'mobile_numbers.id');
            })
            ->leftJoin('mobile_statuses', 'mobile_statuses.id', '=', 'current_status.status_id')
            ->where('mobile_numbers.phone_number', $phoneNumber)
            ->select([
                'mobile_numbers.phone_number',
                DB::raw('COALESCE(mobile_statuses.description, \'available\') as status')
            ])
            ->first();

        return $baseQuery->status;
    }

    /**
     * Check if a phone number is already subscribe to a service
     *
     * @param [type] $mobileId
     * @param [type] $subscription
     * @return void
     */
    public function checkPhoneSubscription($mobileId, $subscription)
    {
        $data = MobileSubscription::lockForUpdate()
                                ->where('mobile_id', $mobileId)
                                ->where('subscription_type_id', $subscription)
                                ->first();
        return $data;
    }

    /**
     * Cancell all existing active mobile subscription
     *
     * @param [type] $phoneNumber
     * @return void
     */
    public function getPhoneSubscription($phoneNumber)
    {
        $mobileNumber = MobileNumber::where('phone_number', $phoneNumber)->first(['id']);
        $data         = MobileSubscription::lockForUpdate()
            ->where('mobile_id', $mobileNumber->id)
            ->where('status_id', '!=', MobileSubscriptionStatus::STATUS_CANCELLED)
            ->get();
        return $data;
    }

    /**
     * Return phone number subscriptions and plans details
     *
     * @param [type] $phoneNumber
     * @return void
     */
    public function getPhoneNumberDetails($phoneNumber)
    {
        $baseQuery = MobileNumber::leftJoin('mobile_subscriptions', function ($join) {
            $join->on('mobile_subscriptions.mobile_id', '=', 'mobile_numbers.id')
                        ->whereNull('mobile_subscriptions.deleted_at');
        })
            ->leftJoin('mobile_services', 'mobile_services.id', '=', 'mobile_subscriptions.subscription_type_id')
            ->leftJoin('mobile_subscription_statuses', 'mobile_subscription_statuses.id', '=', 'mobile_subscriptions.status_id')
            ->leftJoin(DB::raw('(select * from mobile_status_histories where is_current = 1) as current_status'), function ($join) {
                $join->on('current_status.mobile_id', '=', 'mobile_numbers.id');
            })
            ->leftJoin('mobile_statuses', 'mobile_statuses.id', '=', 'current_status.status_id')
            ->select([
                'mobile_numbers.phone_number',
                DB::raw('mobile_statuses.description as mobile_status'),
                DB::raw('current_status.updated_at as mobile_status_update'),
                'mobile_services.subscription_type',
                'mobile_services.description',
                DB::raw('mobile_subscription_statuses.description as subscription_status'),
                DB::raw('mobile_services.updated_at as subscription_status_update'),
            ])
            ->where('mobile_numbers.phone_number', $phoneNumber)
            ->get();

        return $baseQuery;
    }

    /**
     * Returns full history of a phone number's status and subscriptions
     *
     * @param [type] $phoneNumber
     * @return void
     */
    public function getPhoneNumberHistory($phoneNumber)
    {
        $baseQuery = MobileNumber::leftJoin('mobile_subscriptions', 'mobile_subscriptions.mobile_id', '=', 'mobile_numbers.id')
                    ->leftJoin('mobile_services', 'mobile_services.id', '=', 'mobile_subscriptions.subscription_type_id')
                    ->leftJoin('mobile_subscription_statuses', 'mobile_subscription_statuses.id', '=', 'mobile_subscriptions.status_id')
                    ->leftJoin('mobile_status_histories', 'mobile_status_histories.mobile_id', '=', 'mobile_numbers.id')
                    ->leftJoin('mobile_statuses', 'mobile_statuses.id', '=', 'mobile_status_histories.status_id')
                    ->where('mobile_numbers.phone_number', $phoneNumber)
                    ->select([
                        'mobile_numbers.phone_number',
                        DB::raw('mobile_statuses.description as mobile_status'),
                        DB::raw('mobile_status_histories.updated_at as mobile_status_update'),
                        DB::raw('mobile_services.description as subscription'),
                        'mobile_services.subscription_type',
                        DB::raw('mobile_subscription_statuses.description as subscription_status'),
                        DB::raw('mobile_services.updated_at as subscription_status_update'),

                    ])
                    ->orderBy('mobile_status_histories.id', 'DESC')
                    ->orderBy('mobile_status_histories.updated_at', 'DESC')
                    ->get();
        return $baseQuery;
    }
}
