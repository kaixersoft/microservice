<?php

namespace MyRepublic\Mobile\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Model that holdes history changes in status of a mobile number
 */
class MobileStatusHistory extends Model
{
    protected $fillable = [
        'mobile_id', 'status_id', 'is_current'
    ];

    /**
     * Status description
     *
     * @return void
     */
    public function status()
    {
        return $this->hasOne(MobileStatus::class, 'id', 'status_id');
    }
}
