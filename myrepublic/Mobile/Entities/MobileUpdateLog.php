<?php

namespace MyRepublic\Mobile\Entities;

use Illuminate\Database\Eloquent\Model;

class MobileUpdateLog extends Model
{
    protected $fillable = [
        'username',
        'transaction',
        'from_data',
        'to_data'
    ];
}
