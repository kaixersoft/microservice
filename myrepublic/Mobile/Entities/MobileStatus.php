<?php

namespace MyRepublic\Mobile\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Mobile Statuses
 */
class MobileStatus extends Model
{
    const STATUS_AVAILABLE  = 1;
    const STATUS_ACTIVE     = 2;
    const STATUS_TERMINATED = 3;
    protected $fillable     = ['description'];
}
