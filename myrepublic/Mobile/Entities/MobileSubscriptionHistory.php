<?php

namespace MyRepublic\Mobile\Entities;

use Illuminate\Database\Eloquent\Model;

class MobileSubscriptionHistory extends Model
{
    protected $fillable = [
        'mobile_id', 'subscription_type_id', 'status_id', 'admin_username'
    ];
}
