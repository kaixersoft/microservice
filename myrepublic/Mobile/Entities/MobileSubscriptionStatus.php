<?php

namespace MyRepublic\Mobile\Entities;

use Illuminate\Database\Eloquent\Model;

class MobileSubscriptionStatus extends Model
{
    const STATUS_ACTIVE    = '1';
    const STATUS_PENDING   = '2';
    const STATUS_CANCELLED = '3';

    protected $fillable = ['description'];
}
