<?php

namespace MyRepublic\Mobile\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Model that holds all mobile numbers and their table relationships
 *
 */
class MobileNumber extends Model
{
    protected $hidden   = ['id', 'created_at',  'deleted_at'];
    protected $fillable = [];

    /**
     * Mobile statuses
     *
     * @return void
     */
    public function history()
    {
        return $this->hasMany(MobileStatusHistory::class, 'mobile_id', 'id');
    }
}
