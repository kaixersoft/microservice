<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileStatusHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_status_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mobile_id')->index();
            $table->unsignedInteger('status_id')->index();
            $table->boolean('is_current')->index();
            $table->timestamps();

            $table->foreign('mobile_id')
                ->references('id')
                ->on('mobile_numbers');

            $table->foreign('status_id')
                ->references('id')
                ->on('mobile_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_status_histories');
    }
}
