<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mobile_id')->index();
            $table->unsignedInteger('subscription_type_id')->index();
            $table->unsignedInteger('status_id')->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('mobile_id')
                ->references('id')
                ->on('mobile_numbers');

            $table->foreign('subscription_type_id')
                ->references('id')
                ->on('mobile_services');

            $table->foreign('status_id')
                ->references('id')
                ->on('mobile_subscription_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_subscriptions');
    }
}
