<?php

namespace MyRepublic\Mobile\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use MyRepublic\Mobile\Entities\MobileService;

class MobileServicesSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $plans = [
            ['subscription_type' => 'plan', 'description' => 'Smart 35 9GB Data $35 per mo'],
            ['subscription_type' => 'plan', 'description' => 'Mega 55 18GB Data $55 per mo'],
            ['subscription_type' => 'plan', 'description' => 'Xtra 85 30GB Data $85 per mo'],
        ];

        $addons = [
            ['subscription_type' => 'addon', 'description' => 'Data Boosters 500MB $3.50'],
            ['subscription_type' => 'addon', 'description' => 'Data Boosters 1.5GB $7.00'],
            ['subscription_type' => 'addon', 'description' => 'Data Boosters 3GB $12.00'],
            ['subscription_type' => 'addon', 'description' => 'Data Add-ons 1GB $5.00'],
            ['subscription_type' => 'addon', 'description' => 'Data Add-ons 3GB $12.00'],
            ['subscription_type' => 'addon', 'description' => 'Data Add-ons 8GB $30.00'],
        ];

        $data = array_merge($plans, $addons);
        foreach ($data as $entry) {
            MobileService::create($entry);
        }
    }
}
