<?php

namespace MyRepublic\Mobile\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use MyRepublic\Mobile\Entities\MobileNumber;

class MobileDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Populate mobile_numbers table with fake mobile number
        $phoneNumbers = factory(MobileNumber::class, 1000)->create();
    }
}
