<?php

namespace MyRepublic\Mobile\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use MyRepublic\Mobile\Entities\MobileStatus;

class MobileStatusSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $statuses = ['available', 'active', 'terminated'];

        foreach ($statuses as $status) {
            MobileStatus::create(['description' => $status]);
        }
    }
}
