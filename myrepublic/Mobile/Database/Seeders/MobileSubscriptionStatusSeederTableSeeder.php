<?php

namespace MyRepublic\Mobile\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use MyRepublic\Mobile\Entities\MobileSubscriptionStatus;

class MobileSubscriptionStatusSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $statuses = ['active', 'pending', 'cancelled'];

        foreach ($statuses as $status) {
            MobileSubscriptionStatus::create(['description' => $status]);
        }
    }
}
