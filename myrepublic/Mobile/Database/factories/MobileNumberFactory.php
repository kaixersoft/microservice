<?php

use Faker\Generator as Faker;
use MyRepublic\Mobile\Entities\MobileNumber;

$factory->define(MobileNumber::class, function (Faker $faker) {
    return [
        'phone_number' => $faker->numerify('####-####'),
        'created_at'   => Carbon\Carbon::now(),
        'updated_at'   => Carbon\Carbon::now(),
    ];
});
