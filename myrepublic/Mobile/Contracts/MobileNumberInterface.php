<?php

namespace MyRepublic\Mobile\Contracts;

/**
 * Contract for Mobile Number Service
 */
interface MobileNumberInterface
{
    /**
     * Get All mobile number with current status
     *
     */
    public function getAllMobileNumberWithCurrentStatus($request);

    /**
     * Get all details about phone number
     *
     * @param [type] $phoneNumber
     * @return void
     */
    public function getMobileNumberDetails($phoneNumber);

    /**
     * Terminate mobile number
     *
     * @param string $phoneNumber
     * @return void
     */
    public function blockMobileNumber($phoneNumber, $request);

    /**
     * Re-Activate mobile number
     *
     * @param string $phoneNumber
     * @return void
     */
    public function unBlockMobileNumber($phoneNumber, $request);
}
