<?php

namespace MyRepublic\Mobile\Contracts;

/**
 * Cotract class for mobile service
 */
interface MobileServicesInterface
{
    /**
     * Subscribe a given phone number to a service
     *
     * @param [type] $data
     * @return void
     */
    public function subscribe($data);
}
