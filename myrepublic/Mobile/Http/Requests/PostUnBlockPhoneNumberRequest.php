<?php

namespace MyRepublic\Mobile\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use MyRepublic\Mobile\Rules\PhoneNumberMustBeTerminated;
use MyRepublic\Mobile\Rules\PhoneNumberMustExists;

class PostUnBlockPhoneNumberRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'phone_number_must_exists' => [
                new PhoneNumberMustExists($this->phone_number),
                'bail',
                new PhoneNumberMustBeTerminated($this->phone_number),
            ],
            'username' => [
                'required',
                'string'
            ],
        ];
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $this->merge([
            'phone_number_must_exists' => null,
        ]);
        return parent::validationData();
    }
}
