<?php

namespace MyRepublic\Mobile\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use MyRepublic\Mobile\Contracts\MobileServicesInterface;
use MyRepublic\Mobile\Http\Requests\PostMobileNumberSubscribeRequest;

/**
 * Subscribe a mobile number either a plan or add on
 */
class PostMobileNumberSubscribeController extends Controller
{
    /**
     * POST - mobile subscription
     *
     * @param string $phoneNumber
     * @param PostMobileNumberSubscribeRequest $request
     * @return void
     */
    public function __invoke($phoneNumber, PostMobileNumberSubscribeRequest $request)
    {
        $service = resolve(MobileServicesInterface::class);
        try {
            DB::beginTransaction();

            $data   = array_merge_recursive(['phone_number' => $phoneNumber], $request->all());
            $result = $service->subscribe($data);

            DB::commit();

            $message = 'mobile service subscription successfull';
            return response()->json(['message' => $message, 'data' => $result]);
        } catch (\Exception $exception) {
            DB::rollBack();
            $message = $exception->getMessage();
            return response()->json(['messages' => $message], 400);
        }
    }
}
