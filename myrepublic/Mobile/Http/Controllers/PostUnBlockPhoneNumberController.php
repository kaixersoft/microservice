<?php

namespace MyRepublic\Mobile\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use MyRepublic\Mobile\Contracts\MobileNumberInterface;
use MyRepublic\Mobile\Http\Requests\PostUnBlockPhoneNumberRequest;

class PostUnBlockPhoneNumberController extends Controller
{
    /**
     * POST - Re-Activate terminated phone number
     *
     * @param [type] $phoneNumber
     * @param PostUnBlockPhoneNumberRequest $request
     * @return void
     */
    public function __invoke($phoneNumber, PostUnBlockPhoneNumberRequest $request)
    {
        $mobileNumberService = resolve(MobileNumberInterface::class);

        try {
            DB::beginTransaction();

            $result = $mobileNumberService->unBlockMobileNumber($phoneNumber, $request->all());

            DB::commit();

            $message = 'mobile service activated';
            return response()->json(['message' => $message, 'data' => $result]);
        } catch (\Exception $exception) {
            DB::rollback();
            $message = $exception->getMessage();
            return response()->json(['messages' => $message], 400);
        }
    }
}
