<?php

namespace MyRepublic\Mobile\Http\Controllers;

use Illuminate\Routing\Controller;
use MyRepublic\Mobile\Contracts\MobileNumberInterface;
use MyRepublic\Mobile\Http\Requests\GetMobileDetailsRequest;

class GetMobileDetailsController extends Controller
{
    /**
     * Return full details of a phone number subscription and plans
     *
     * @param [type] $phoneNumber
     * @param GetMobileDetailsRequest $request
     * @return void
     */
    public function __invoke($phoneNumber, GetMobileDetailsRequest $request)
    {
        $service = resolve(MobileNumberInterface::class);
        return $service->getMobileNumberDetails($phoneNumber);
    }
}
