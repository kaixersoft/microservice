<?php

namespace MyRepublic\Mobile\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use MyRepublic\Mobile\Contracts\MobileNumberInterface;

/**
 * Fetch all mobile number controller
 */
class GetAllNumberWithStatusController extends Controller
{
    /**
     * Fetch all mobile number with status
     *
     * @param Request $request
     */
    public function __invoke(Request $request)
    {
        $service = resolve(MobileNumberInterface::class);

        return $service->getAllMobileNumberWithCurrentStatus($request->all());
    }
}
