<?php

namespace MyRepublic\Mobile\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use MyRepublic\Mobile\Contracts\MobileNumberInterface;
use MyRepublic\Mobile\Http\Requests\PostBlockMobilePhoneNumberRequest;

/**
 * Terminate phone number service and all subscription
 */
class PostBlockMobilePhoneNumberController extends Controller
{
    /**
     * POST - Block phone number and set status to terminated
     *
     * @param [type] $phoneNumber
     * @param PostBlockMobilePhoneNumberRequest $request
     * @return void
     */
    public function __invoke($phoneNumber, PostBlockMobilePhoneNumberRequest $request)
    {
        $mobileNumberService = resolve(MobileNumberInterface::class);

        try {
            DB::beginTransaction();

            $result = $mobileNumberService->blockMobileNumber($phoneNumber, $request->all());

            DB::commit();

            $message = 'mobile service terminated';
            return response()->json(['message' => $message, 'data' => $result]);
        } catch (\Exception $exception) {
            DB::rollback();
            $message = $exception->getMessage();
            return response()->json(['messages' => $message], 400);
        }
    }
}
