<?php

namespace MyRepublic\Mobile\Http\Controllers;

use Illuminate\Routing\Controller;
use MyRepublic\Mobile\Contracts\MobileNumberInterface;
use MyRepublic\Mobile\Http\Requests\GetPhoneNumberHistoryRequest;

class GetPhoneNumberHistoryController extends Controller
{
    /**
     * GET - full history of phone number status and subscriptions
     *
     * @param [type] $phoneNumber
     * @param GetPhoneNumberHistoryRequest $request
     * @return void
     */
    public function __invoke($phoneNumber, GetPhoneNumberHistoryRequest $request)
    {
        $service = resolve(MobileNumberInterface::class);

        return $service->getMobileNumberHistory($phoneNumber);
    }
}
