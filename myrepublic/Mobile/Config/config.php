<?php

return [
    'name'     => 'Mobile',
    'bindings' => [
        \MyRepublic\Mobile\Contracts\MobileNumberInterface::class       => \MyRepublic\Mobile\Services\MobileNumberService::class,
            \MyRepublic\Mobile\Contracts\MobileServicesInterface::class => \MyRepublic\Mobile\Services\MobileService::class
    ]
];
