<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['client_credentials', 'throttling:100:1'], 'prefix' => 'v1/mobile'], function () {
    Route::get('all', 'GetAllNumberWithStatusController')->name('get.mobile.all');
    Route::post('{phone_number}/subscribe', 'PostMobileNumberSubscribeController')->name('post.mobile.subscribe');
    Route::get('{phone_number}/details', 'GetMobileDetailsController')->name('get.mobile.details');
    Route::post('{phone_number}/block', 'PostBlockMobilePhoneNumberController')->name('post.mobile.block');
    Route::post('{phone_number}/activate', 'PostUnBlockPhoneNumberController')->name('post.mobile.activate');
    Route::get('{phone_number}/history', 'GetPhoneNumberHistoryController')->name('get.mobile.history');
});
