<?php

if (!function_exists('get_builder_sql')) {
    function get_builder_sql($builder)
    {
        # first escape the custom percent in the builder
        $str = str_replace('%', '%%', $builder->toSql());

        # then replace all ? into %s
        $str = str_replace(['?'], ['\'%s\''], $str);

        # now pass in the bindings
        return vsprintf($str, $builder->getBindings());
    }
}
