# Micro Service
Phone number subscription
_______________________
### Environment requirement
- Web server such as Nginx
- MySQL 5.7+
- [composer](https://getcomposer.org/download/) must be installed 
- PHP 7.1 minimum must be installed
_______________________
### Installation
- Clone repository
```sh
  $> git clone https://gitlab.com/kaixersoft/microservice.git folder_name
```

- Copy .env.example to foldername/.env
```sh
  $> cp .env.example .env
```  
- Open .env file and make sure to set the following correctly based on your setup
  - DB_CONNECTION=mysql
  -  DB_HOST={database_host_ip}
  - DB_PORT=3306
  - DB_DATABASE={database_name}
  - DB_USERNAME={database_username}
  - DB_PASSWORD={database_password}
  
- Run following commands inside foldername (this should only be run **once**)
```sh
  $> composer install
  $> composer dumpautoload
  $> php artisan key:generate
  $> php artisan migrate
  $> php artisan passport:install
  $> php artisan passport:client --client --name=myrepublic
  $> php artisan db:seed
```
_______________________
### SPECIFICATIONS
**Conditions**
- Only status=available and active phone numbers are allowed to subscribe to any PLAN
- Only status=active and available phone numbers are allowed to subscribe to any ADD-ONS
- Only status=active phone number are allowed to be terminated
- Only status=terminated phone number are allowed to be available

**API**
- base url http://**{domain}**/api/v1/mobile/**{uri}**
- API is throttled to 100 requests per minute
- Headers :
```
    Accept: application/json
    Content-Type: application/json
    Authorization : Bearer {access_token}
```
- [API documentation](https://gitlab.com/kaixersoft/microservice/wikis/API-Documentation)

**HTTP client**
- [POSTMAN](https://www.getpostman.com/downloads/)

