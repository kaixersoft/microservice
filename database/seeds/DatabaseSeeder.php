<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Contains seeder classes
     *
     * @var array
     */
    protected $appSeeders = [
        \MyRepublic\Mobile\Database\Seeders\MobileDatabaseSeeder::class,
        \MyRepublic\Mobile\Database\Seeders\MobileStatusSeederTableSeeder::class,
        \MyRepublic\Mobile\Database\Seeders\MobileSubscriptionStatusSeederTableSeeder::class,
        \MyRepublic\Mobile\Database\Seeders\MobileServicesSeederTableSeeder::class,
    ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $batch = $this->getNextBatchNumber();
        $count = 0;
        foreach ($this->getSeeders() as $seeder) {
            if (!$this->exists($seeder)) {
                $this->call($seeder);
                $this->log($seeder, $batch);
                ++$count;
            }
        }

        if (!$count) {
            echo "Nothing to seed \n";
        }
    }

    /**
     * Log seeder.
     *
     * @param unknown $seeder
     * @param unknown $batch
     *
     * @return unknown
     */
    protected function log($seeder, $batch)
    {
        return \DB::table('seeders')->insert(['seeder' => $seeder, 'batch' => $batch]);
    }

    /**
     * Get next batch number.
     *
     * @return number
     */
    protected function getNextBatchNumber()
    {
        return \DB::table('seeders')->max('batch') + 1;
    }

    /**
     * Check if seeder exists.
     *
     * @param unknown $seeder
     *
     * @return unknown
     */
    protected function exists($seeder)
    {
        return \DB::table('seeders')->where('seeder', $seeder)->exists();
    }

    /**
     * Get seeders.
     *
     * @return string[]
     */
    protected function getSeeders()
    {
        return $this->appSeeders;
    }
}
